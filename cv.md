---
name: "Hamilton Greene"
role: "Software Engineer"
tagline: "I build dope shit."
site: "https://iamhamy.xyz"
portfolio: [ "https://labs.iamhamy.xyz", "https://gitlab.com/sirhamy" ]
contact: "yo@iamhamy.xyz"
---

# Experience

## Work

### Applied Predictive Technologies (acquired by MasterCard)

```
role: "software engineer"
technologies: [ "c#", "sql", "javascript", "react", ".net" ]
startDate: 2017.02
```

* Software Engineer - Architecture (core services)
    * Worked on team building and maintaining platform core services like authentication / login, asynchronous message queues, toggles, logging, exception / error monitoring, etc.
    * Increased Job Queue farming query performance by 90% by optimizing intermediate query calculations via clustered index and batch farming, achieving ~10x increase in platform scalability
    * Implemented core APIs using .NET MVC to allow safe access/modification to core user data
    * Built single sign on integration with a third party platform leveraging JSON Web Tokens
    * Built self-service password reset flow (I forgot my password) using Okta APIs in a paradigm that leaked minimal user data / access to the platform
* Software Engineer - DISCo (Data Import, Setup, and Configuration operations)
    * Led engineering vision, roadmap, support, and feature development for service used to import and maintain ~85% of warehoused client data
    * Played lead role in gathering data on and solving for pain points / goals of consuming teams
    * Helped architect and build a new service from the ground-up with intense data-security design constraints
    * Created and promoted office-wide engineering learning / engagement events to facilitate cross-pollination of ideas and transparency to wide-spread obstacles

### IBM

```
role: "front end developer (intern)"
technologies: [ "python", "react", "nodejs", "IBM Bluemix/CloudFoundry/Watson" ]
startDate: 2016.05
endDate: 2016.08
```

* Frontend developer
    * Worked with multidisciplinary team of designers to define and solve for issues in the corporate training / knowledge space
    * Sole developer of full-stack, multi-tier learning platform hosted on IBM cloud, using IBM Graph for data storage, service written in Python Flask for data tier, and a React app served with NodeJS Express for web tier

### For more...

Visit [my LinkedIn](https://www.linkedin.com/in/hamiltongreene/).

## Education

### Georgia Institute of Technology

* BS Computer Science (2016)
    * Information Internetworks
    * Media

## Personal Projects (https://labs.iamhamy.xyz/projects)

### iamhamy.xyz

```
technologies: [ "hugo", "kubernetes", "google-cloud-engine", "gitlab-ce" ]
```

Created and deployed iamhamy.xyz et al. on Google Kubernetes Engine. Deploys using build hooks in GitLab CE, which constructs Docker containers before, publishing to my Kubernetes instance. You can read more about how it works [in the announcement](https://labs.iamhamy.xyz/posts/labs-hosted-on-gke/).

To explore:
* https://iamhamy.xyz
* https://blog.iamhamy.xyz
* https://labs.iamhamy.xyz

### moon-eye 

```
technologies: [ "javascript", "web-audio-api" ]
link: https://labs.iamhamy.xyz/projects/moon-eye/
```

Audio visualizer leveraging the Web Audio API. [Play with the demo](https://labs.iamhamy.xyz/projects/moon-eye/).